CHANGELOG
===

This changelog contains all notable change of this project

1.0.9
---
1. Change the format of the validation errors to be compatible with the JS format.

1.0.8
---
1. Record function: Use the self link from the record only when it is not presented in the main chain.

1.0.7
---
1. Now Collection will convert \Illuminate\Pagination\LengthAwarePaginator to \Illuminate\Support\Collection for easy 
handling.
2. Now Collection will always return Array inside _embedded element.

1.0.6
---
1. Update the collection function to accept either Collections or Array.
2. The system will auto try to pluralize the relations key names when passed to collection.
3. Now the Record accept either Array or Eloquent\Model.
4. The system will auto remove any relations within Eloquent Model when you pass it to record.

1.0.5
---
1. Using Binds instead of singleton to make sure we get a fresh data.


1.0.4
---
1. Adding some simple exception handlers.

1.0.3
---
1. Adding a nicer message with the validation section.

1.0.2
---
1. Adding the validation message section.

1.0.1
---
1. Adding more tests.

1.0.0
---
1. Adding more tests.

0.1.3
---
1. Make sure we expose only the links public function
2. Start implementing tests.

0.1.2
---
1. Make sure we return the relative path.
2. document the `hal()` helper function

0.1.0
---
1. The first release.