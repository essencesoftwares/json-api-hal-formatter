HAL Specification formatter
===

This package will help you return your API response as specified in the HAL Specification, with simple functionality.

_Note_: This may not be the perfect solution, but we will have to work on it to enhance it as we work.

Installation
---

1. Then add it to your require section either using composer require

```bash
composer require "essence/hal-formatter:~1.0" --prefer-source
```

or manually by editing your `composer.json` file like:

```json
    {
        "require":{
            "essence/hal-formatter":"~1.0"
        }
    }
```

for sure your require section will have more packages not just this one, so remember to *add* it.

Usage
---

You can use the `Hal` like:

```php
return response()->json(Hal::serviceName('post')->get());
```

Which will return the minimum response, or if you like to use the `hal()` helper function which will return the same

```php
return response()->json(hal('post')->get());
```

Check the docs folder for all the information about how to use it with Laravel Eloquent.




Changelog
---
Check [CHANGELOG](CHANGELOG.md) for the changelog

Testing
---


Contributing
---
*Information will follow soon*


Security
---
If you discover any security related issues, please email developer@klassapp.com or use the issue tracker of Bitbuket.


License
---
The MIT License (MIT). Please see [License File](LICENSE)
for more information.