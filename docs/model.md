# The updates for the model.

Whenever you want to use the Hal formatter with Eloquent model to automatically add the `_links` attribute
you will have to use the `HalModel` trait, this will make your life more easier.

So lets assume we have a `Post` model like the following

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Essence\Hal\Traits\HalModel;

class Post extends Model
{
    use HalModel;
}
```

If we didnt use the `HalModel` trait, the model will return the data like the following:

```json
{
    "id": 1,
    "title": "Elliott Ratke",
    "article": "milford.fritsch@example.com",
    "created_at": "2017-11-07 09:42:41",
    "updated_at": "2017-11-07 09:42:41"
}
```

Meanwhile if we are using the `HalModel` trait, the returned value will be like the following:

```json
{
    "id": 1,
    "title": "Elliott Ratke",
    "article": "milford.fritsch@example.com",
    "created_at": "2017-11-07 09:42:41",
    "updated_at": "2017-11-07 09:42:41",
    "_links": {
        "self": {
            "href": "/posts/1"
        }
    }
}
```

Automatically the `HalModel` trait will use the name of the table, in our case here its `posts`, as the base for the
`href` element. If you want to change this base you can add a new protected variable called `path` so your model for 
will look like this for example:

```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Essence\Hal\Traits\HalModel;

class Posts extends Model
{
    use HalModel;

    protected $linkPath = 'master-posts';
}
```

And the results will be like:

```json
{
    "id": 1,
    "title": "Elliott Ratke",
    "article": "milford.fritsch@example.com",
    "created_at": "2017-11-07 09:42:41",
    "updated_at": "2017-11-07 09:42:41",
    "_links": {
        "self": {
            "href": "/master-posts/1"
        }
    }
}
```

## Relations

For the time, it would be great if you named your relations in plural instead of singular all the time, for example, if 
the post has an author, we will have to call it `users` as the path for the user record will be `/users/1` so the relation 
function should be (for now):

```php
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
```  