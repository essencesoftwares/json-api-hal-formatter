# Other functionality

We have few functions which we can use to return the data in case of creating or updating a record, or in case we want to 
return an error message.

## Default response

By default using the following minimum code:
```php
    return response()->json(Hal::get());
```

We get back the following response:
```json
{
    "_links": {
        "self": {
            "href": "/"
        }
    },
    "success": true
}
```

As we didnt specify the url the default one will be the requested URI.

## Returning a message:

To return a message you pass the message to the `message` function

```php
    $response = Hal::seviceName('post')
        ->links('/this-route-is-invalid')
        ->success(false)
        ->message('Specified route is invalid');
        
    return response()->json($response, 404);        
```

And the returned response will be:

```json
{
    "_links": {
        "self": {
            "href": "/this-route-is-invalid"
        }
    },
    "message": "Specified route is invalid",
    "success": false
}
```

## Returning data

Not all functions need the `get` function to return data and those functions are:

1. message
2. pagination
3. record

And if you didnt use the `get` function, the class will fallback to `__toString` function.

## Return response for a create request

Lets say you created a new record, so to return the response you will do the following.

```php

    $post = Posts::create([
        'title' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        'article' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        'user_id' => 1
    ]);
    
    $response = Hal::seviceName('post')
        ->links('/posts/'. $post->id)
        ->attributes($post->toArray())
        ->get();
    
    return response()->json($response);

```

and the returned response is:

```json
{
    "_links": {
        "self": {
            "href": "/posts/47"
        }
    },
    "attributes": {
        "title": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "article": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "updated_at": "2017-11-08 11:27:30",
        "created_at": "2017-11-08 11:27:30",
        "id": 47,
        "user_id": 1        
    },
    "success": true
}

```

## Return response for a update request

Lets say you updated a record, so to return the response you will do the following.

```php

    $post = Posts::find(47);
    $oldRecord = $post->getOriginal();
    
    $post->title = 'new: Lorem Ipsum is simply dummy text of the printing and typesetting industry.';
    $post->article = 'updated: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
    $post->user_id = 3;

    $post->save();


    $response = Hal::seviceName('post')
        ->links('/posts/'. $post->id)
        ->attributes($post->toArray())
        ->previousAttributes($oldRecord)
        ->get();
    
    return response()->json($response);

```

and the returned response is:

```json
{
    "_links": {
        "self": {
            "href": "/posts/47"
        }
    },
    "_previousAttributes": {
        "id": 47,
        "user_id": 3,
        "title": "Old: Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "article": "updated: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2017-11-08 11:27:30",
        "updated_at": "2017-11-08 11:53:36"
    },
    "attributes": {
        "id": 47,
        "user_id": 2,        
        "title": "new: Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "article": "updated: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "created_at": "2017-11-08 11:27:30",
        "updated_at": "2017-11-08 11:55:19"
    },
    "success": true
}

```