# The usage to return the record as HAL

Please make sure that you have read the [model](docs/model.md) file before you work with this section.

## Usage without relation

Now that you have your record, you will need to return the response back to your users, to do so you will have to use
the `HAL` facade with few more methods.

Lets look at the following code:

```php
    $post = Posts::find(1);
    
    $response = Hal::serviceName('post')
        ->links(Request::getRequestUri())
        ->success(true)
        ->record($post->toArray());
        
    return response()->json($response);
```

The response will sent back to the browser like the following:

```json
{
    "success": true,
    "_links": {
        "self": {
            "href": "/posts/1"
        }
    },
    "id": 1,
    "title": "Elliott Ratke",
    "article": "milford.fritsch@example.com",
    "created_at": "2017-11-07 09:42:41",
    "updated_at": "2017-11-07 09:42:41"
}
```

## Usage with relation

Now lets say that our `Post` class has a relationship with the `User` model class, and we already defined the relations 
so our code will be like this:

```php
    $post = Posts::with('user')->find(1);
    
    $response = Hal::serviceName('post')
        ->links(Request::getRequestUri())
        ->success(true)
        ->collection($post->user->toArray(), 'users')
        ->record(array_except($post->toArray(), 'user'));
        
    return response()->json($response);
```

Please note that we have used the `array_except` function to exclude the `user` attribute from the `post` since Eloquent 
will return the relation as part of the attributes. It does not really matter if you load your relation with `load` or `with` 
as in both cases you will need to exclude the relation attribute (based on what you have named it).

The `collection` function will accept one required parameter which is the Eloquent Collection that we need to add to the 
`embedded` section, and the second optional parameter is the *key* to this collection, by default the *key* will be equal to 
the service name, and we will see later why, but since we are adding the relations here we used a different *key*.

And the result which will be sent to the browser will be:

```json
{
    "success": true,
    "_links": {
        "self": {
            "href": "/posts/1"
        }
    },
    "embedded": {
        "users": [
            {
                "id": 1,
                "name": "Amalia Howe",
                "email": "ttorp@example.com",
                "created_at": "2017-11-07 09:42:40",
                "updated_at": "2017-11-07 09:42:40",
                "_links": {
                    "self": {
                        "href": "/users/1"
                    }
                }
            }
        ]
    },
    "id": 1,
    "title": "Elliott Ratke",
    "article": "milford.fritsch@example.com",
    "created_at": "2017-11-07 09:42:41",
    "updated_at": "2017-11-07 09:42:41"
}
```

We are not limited to only one collection, we can add as many as we want, 
as long as you use a different key as the second parameter.