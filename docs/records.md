# The usage to return full set of records as HAL

Please make sure that you have read the [model](/docs/model.md) and [record](/docs/record.md) file before you work with this section.

We already saw how to return the data as HAL when dealing with one record, and the way we do it for a set of records is the same.

Lets look at the following code:

```php
    $posts = Posts::with('users')->paginate($perPage);
    
    $response = Hal::serviceName('post')
        ->links(
            Request::getRequestUri(),
            $posts->url(1),
            $posts->nextPageUrl(),
            $posts->previousPageUrl(),
            $posts->url($users->lastPage())
        )
        ->success(true)
        ->collection($posts->getCollection())
        ->pagination(
            $posts->total(),
            $posts->lastPage(),
            $posts->currentPage(),
            $posts->perPage()
        );
        
    return response()->json($response);
```

The response will sent back to the browser like the following:

```json
{
    "_links": {
        "self": {
            "href": "/posts"
        },
        "first": {
            "href": "/posts?page=1"
        },
        "next": {
            "href": "/posts?page=2"
        },
        "last": {
            "href": "/posts?page=20"
        }
    },
    "embedded": {
        "posts": [
            {
                "id": 1,
                "title": "Elliott Ratke",
                "article": "milford.fritsch@example.com",
                "created_at": "2017-11-07 09:42:41",
                "updated_at": "2017-11-07 09:42:41",
                "_links": {
                    "self": {
                        "href": "/posts/1"
                    }
                },
                "users": {
                    "id": 1,
                    "name": "Amalia Howe",
                    "email": "ttorp@example.com",
                    "created_at": "2017-11-07 09:42:40",
                    "updated_at": "2017-11-07 09:42:40",
                    "_links": {
                        "self": {
                            "href": "/users/1"
                        }
                    }
                }
            },
            {
                "id": 2,
                "title": "Marisol Bartell II",
                "article": "vdaniel@example.com",
                "created_at": "2017-11-07 09:42:41",
                "updated_at": "2017-11-07 09:42:41",
                "_links": {
                    "self": {
                        "href": "/posts/2"
                    }
                },
                "users": {
                    "id": 10,
                    "name": "Dr. Jacquelyn Halvorson III",
                    "email": "walsh.andreanne@example.net",
                    "created_at": "2017-11-07 09:42:40",
                    "updated_at": "2017-11-07 09:42:40",
                    "_links": {
                        "self": {
                            "href": "/users/10"
                        }
                    }
                }
            }
        ]
    },
    "name": "posts",
    "page": 1,
    "pageCount": 20,
    "pageSize": 2,
    "success": true,
    "total": 40
}
```