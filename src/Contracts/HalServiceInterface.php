<?php

namespace Essence\Hal\Contracts;

interface HalServiceInterface
{
    /**
     * @param $name
     * @return mixed
     */
    public function serviceName($name);

    /**
     * @return string
     */
    public function getName() :string;

    /**
     * This function should return the links
     *
     * @param string $self
     * @param string|null  $first
     * @param string|null  $next
     * @param string|null  $previous
     * @param string|null  $last
     * @return mixed
     */
    public function links(
        string $self,
        string $first = null,
        string $next = null,
        string $previous = null,
        string $last = null
    );


    /**
     * This function should return the embbeded content.
     *
     * @param array|\Illuminate\Support\Collection $records : The database records.
     * @param string|null $key : The key to be used for the namespace.
     * @return mixed
     */
    public function collection($records, string $key = null);

    /**
     * This function should return the record content.
     *
     * @param  array|\Illuminate\Database\Eloquent\Model $record : The database record
     * @return array
     */
    public function record($record) : array;


    /**
     * This function should return the pagination meta data.
     *
     * @param int $total : Total number of records.
     * @param int $pageCount : Total number of pages.
     * @param int $page : Current page number.
     * @param int $pageSize : The total number of records per page.
     * @return array
     */
    public function pagination(int $total = 0, int $pageCount = 0, int $page = 0, int $pageSize = 20) :array;


    /**
     * This function should return the attributes data.
     *
     * @param array $data : The attributes data.
     * @return mixed
     */
    public function attributes(array $data);


    /**
     * This function should return the previous attributes data.
     *
     * @param array $data : The previous attributes data.
     * @return mixed
     */
    public function previousAttributes(array $data);

    /**
     * @param bool $success
     * @return mixed
     */
    public function success($success = true);

    /**
     * @param string $message
     * @return mixed
     */
    public function message(string $message);

    /**
     * @param array $message
     * @return mixed
     */
    public function validation(array $message);

    /**
     * @return array
     */
    public function get() :array;
}
