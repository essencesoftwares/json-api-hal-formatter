<?php

namespace Essence\Hal\Exceptions;

use Exception;

class CannotReturnPaginationException extends Exception
{
}
