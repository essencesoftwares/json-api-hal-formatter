<?php

namespace Essence\Hal\Exceptions;

class MissingHeaders extends \Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(
            hal()
                ->success(false)
                ->links($request->getRequestURI())
                ->message($this->getMessage()),
            $this->getCode() ?? 400
        );
    }
}
