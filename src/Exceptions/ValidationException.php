<?php

namespace Essence\Hal\Exceptions;

class ValidationException extends \Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json(
            hal()
                ->success(false)
                ->links($request->getRequestURI())
                ->validation(json_decode($this->getMessage(), true)),
            400 // make sure that the validation will always throw 400
        );
    }
}
