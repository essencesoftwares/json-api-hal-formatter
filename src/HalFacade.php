<?php
namespace Essence\Hal;

use Illuminate\Support\Facades\Facade;

class HalFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'hal';
    }
}
