<?php

namespace Essence\Hal;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Essence\Hal\Contracts\HalServiceInterface;
use Essence\Hal\Exceptions\CannotReturnPaginationException;

class HalService implements HalServiceInterface
{
    /**
     * @var
     */
    protected $name;

    /**
     * @var array
     */
    protected $fullChain = ['success' => true, '_links' => []];

    /**
     * @var bool
     */
    protected $hasAttribute = false;

    /**
     * @param $name
     * @return $this
     */
    public function serviceName($name)
    {
        $this->name = Str::plural($name);
        return $this;
    }

    /**
     * @return string
     */
    public function getName() :string
    {
        return Str::plural($this->name);
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function success($success = true)
    {
        $this->fullChain['success'] = is_bool($success) ? $success : ($success === 'true');

        return $this;
    }

    /**
     * This function should return the links
     *
     * @param string $self : Current link
     * @param string|null  $first : first link
     * @param string|null  $next : next link
     * @param string|null  $previous : previous link
     * @param string|null  $last : last link
     * @return $this
     */
    public function links(
        string $self,
        string $first = null,
        string $next = null,
        string $previous = null,
        string $last = null
    ) {
        $this->addCurrentLink($self);
        $this->addFirstLink($first);
        $this->addNextLink($next);
        $this->addPreviousLink($previous);
        $this->addLastLink($last);

        return $this;
    }

    /**
     * @param string|null $self
     * @return $this
     */
    protected function addCurrentLink(string $self = null)
    {
        if (null !== $self) {
            $this->fullChain['_links']['self']['href'] = $self;
        }
        
        return $this;
    }

    /**
     * @param string|null $first
     * @return $this
     */
    protected function addFirstLink(string $first = null)
    {
        if (null !== $first) {
            $this->fullChain['_links']['first']['href'] = str_replace(url('/'), '', $first);
        }

        return $this;
    }

    /**
     * @param string|null $last
     * @return $this
     */
    protected function addLastLink(string $last = null)
    {
        if (null !== $last) {
            $this->fullChain['_links']['last']['href'] = str_replace(url('/'), '', $last);
        }

        return $this;
    }

    /**
     * @param string|null $next
     * @return $this
     */
    protected function addNextLink(string $next = null)
    {
        if (null !== $next) {
            $this->fullChain['_links']['next']['href'] = str_replace(url('/'), '', $next);
        }

        return $this;
    }

    /**
     * @param string|null $previous
     * @return $this
     */
    protected function addPreviousLink(string $previous = null)
    {
        if (null !== $previous) {
            $this->fullChain['_links']['previous']['href'] = str_replace(url('/'), '', $previous);
        }

        return $this;
    }

    /**
     * This function should return the record content.
     *
     * @param array|\Illuminate\Database\Eloquent\Model $record : The database record
     * @return array
     */
    public function record($record) : array
    {
        if ($record instanceof \Illuminate\Database\Eloquent\Model) {
            $keys = array_keys($record->getRelations());
            foreach ($keys as $key) {
                unset($record[$key]);
            }

            $record = $record->toArray();
        }

        $this->fullChain['_links'] = array_only($this->fullChain['_links'], 'self');

        if (count($this->fullChain['_links']) === 1) {
            $record = array_except($record, '_links');
        }

        return array_merge($this->fullChain, $record);
    }

    /**
     * This function should return the embbeded content.
     *
     * @param array|\Illuminate\Support\Collection $records : The database records.
     * @param string|null $key : The key to be used for the namespace.
     * @return $this
     */
    public function collection($records, string $key = null)
    {
        if ($records instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $records = $records->getCollection();
        }

        if ($records instanceof \Illuminate\Support\Collection) {
            $records->each(function ($item) {
                $keys = array_keys($item->getRelations());
                foreach ($keys as $key) {
                    $item[Str::plural($key)] = $item[$key];
                    if (Str::plural($key) !== $key) {
                        unset($item[$key]);
                    }
                }
            });
            $records = $records->toArray();
        }


        if ($records instanceof \Illuminate\Database\Eloquent\Model) {
            $records = $records->toArray();
        }

        $key = $key ?? $this->getName();

        $this->fullChain['_embedded'][Str::plural($key)]  = $records;

        return $this;
    }

    /**
     * This function should return the pagination meta data.
     *
     * @param int $total : Total number of records.
     * @param int $pageCount : Total number of pages.
     * @param int $page : Current page number.
     * @param int $pageSize : The total number of records per page.
     * @return array
     */
    public function pagination(int $total = 0, int $pageCount = 0, int $page = 0, int $pageSize = 20) : array
    {
        $this->validateAttribute();

        $this->fullChain = array_merge($this->fullChain, [
            'name' => $this->name,
            'total' =>  $total,
            'pageCount' =>  $pageCount,
            'page' =>  $page,
            'pageSize' =>  $pageSize
        ]);

        ksort($this->fullChain);

        return $this->fullChain;
    }

    /**
     * This function should return the attributes data.
     *
     * @param array $data : The attributes data.
     * @return $this
     */
    public function attributes(array $data)
    {
        $this->fullChain['attributes'] = array_except($data, '_links');
        $this->hasAttribute = true;

        return $this;
    }

    /**
     * This function should return the previous attributes data.
     *
     * @param array $data : The previous attributes data.
     * @return $this
     */
    public function previousAttributes(array $data)
    {
        $this->fullChain['_previousAttributes'] = array_except($data, '_links');
        $this->hasAttribute = true;

        return $this;
    }

    /**
     * @return array
     */
    public function get() :array
    {
        $this->validateLinks();
        
        ksort($this->fullChain);

        return $this->fullChain;
    }

    /**
     * @param string $message
     * @return array
     */
    public function message(string $message) : array
    {
        $this->validateAttribute();

        $this->fullChain['message'] = $message;

        ksort($this->fullChain);

        return $this->fullChain;
    }

    /**
     * @param array $message
     * @return array
     * @throws CannotReturnPaginationException
     */
    public function validation(array $message) : array
    {
        $this->validateAttribute();

        $this->fullChain['messages']['body'] = $message;

        ksort($this->fullChain);

        return $this->fullChain;
    }

    /**
     * @throws CannotReturnPaginationException
     */
    protected function validateAttribute()
    {
        if ($this->hasAttribute) {
            throw new CannotReturnPaginationException(
                sprintf(
                    'You cant use the %s function with attributes or/and previous attribute function',
                    debug_backtrace()[1]['function']
                ),
                500
            );
        }
        $this->validateLinks();
    }

    /**
     *
     */
    protected function validateLinks()
    {
        if (!array_has($this->fullChain, '_links')) {
            $this->addCurrentLink(request()->getRequestUri());
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        ksort($this->fullChain);
        return json_encode($this->fullChain, JSON_UNESCAPED_SLASHES);
    }
}
