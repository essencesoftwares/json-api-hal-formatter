<?php

namespace Essence\Hal;

use Illuminate\Support\ServiceProvider;

class HalServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->bind('hal', function () {
            return new HalService();
        });

        $this->app->alias(HalService::class, 'Hal');
    }

    public function provides()
    {
        return ['hal'];
    }
}
