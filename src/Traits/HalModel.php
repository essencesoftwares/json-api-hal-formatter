<?php

namespace Essence\Hal\Traits;

use Illuminate\Support\Str;

trait HalModel
{
    /**
     * This function will auto appends the _links to the Model attributes
     *
     * @return mixed
     */
    protected function getArrayableAppends()
    {
        $this->appends = array_unique(array_merge($this->appends, ['_links']));

        return parent::getArrayableAppends();
    }

    /**
     * This function will get the _links attributes
     *
     * @return mixed
     */
    public function getLinksAttribute()
    {
        $this->attributes['_links'] = [
            'self' => [
                'href' => "/{$this->getLinkPath()}/{$this->id}"
            ]
        ];

        return $this->attributes['_links'];
    }

    /**
     * This function will return the path that we will use in _links
     *
     * @return string
     */
    protected function getLinkPath()
    {
        $path = $this->linkPath ?? $this->getTable();
        return Str::plural($path);
    }
}
