<?php

if (! function_exists('hal')) {
    function hal($serviceName = null)
    {
        if (null === $serviceName) {
            return app('hal');
        }

        return app('hal')->serviceName($serviceName);
    }
}
