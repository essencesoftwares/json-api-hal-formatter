<?php

use Illuminate\Support\Str;
use App\Posts;

class HalResponseTest extends TestCase
{
    public function testItReturnDefaultValues()
    {
        $response = Hal::get();

        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('success', $response);
        $this->assertTrue($response['success']);
    }

    public function testItReturnCorrectServiceName()
    {
        $serviceName = Hal::serviceName('user')->getName();

        $this->assertSame(Str::plural('user'), $serviceName);
    }

    public function testItReturnAttributes()
    {
        $response = Hal::serviceName('user')
            ->attributes([
                'id' => 1,
                'name' => 'test',
                'description' => 'something'
            ])->get();


        $this->assertArrayHasKey('attributes', $response);
        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('_links', $response);
        $this->assertTrue($response['success']);
        $this->assertArraySubset(['id' => 1,  'name' => 'test', 'description' => 'something'], $response['attributes']);
    }

    public function testItReturnPreviousAttributes()
    {
        $response = Hal::serviceName('user')
            ->attributes([
                'id' => 2,
                'name' => 'update',
                'description' => 'something has been updated'
            ])
            ->previousAttributes([
                'id' => 1,
                'name' => 'test',
                'description' => 'something'
            ])
            ->get();


        $this->assertArrayHasKey('attributes', $response);
        $this->assertArrayHasKey('_previousAttributes', $response);
        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('_links', $response);
        $this->assertTrue($response['success']);
        $this->assertArraySubset(['id' => 2,  'name' => 'update', 'description' => 'something has been updated'], $response['attributes']);
        $this->assertArraySubset(['id' => 2,  'name' => 'update', 'description' => 'something has been updated'], $response['attributes']);
        $this->assertArraySubset(['id' => 1,  'name' => 'test', 'description' => 'something'], $response['_previousAttributes']);
    }

    public function testItReturnSuccessFalse()
    {
        $response = Hal::success(false)->get();

        $this->assertArrayHasKey('success', $response);
        $this->assertFalse($response['success']);
    }

    public function testItReturnOneRecord()
    {
        $response = Hal::serviceName('user')
            ->links('/')
            ->record([
                'id' => 1,
                'name' => 'No idea',
                'description' => 'This is just a test'
            ]);


        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('description', $response);
        $this->assertArraySubset(['id' => 1, 'name' => 'No idea', 'description' => 'This is just a test'], $response);
    }

    public function testItWillHandelRecords()
    {
        $perPage = 2;
        $pageCount = 50/ $perPage;
        factory(Posts::class, 50)->create();
        $post = Posts::with('user')->paginate($perPage);

        $response = Hal::serviceName('post')
            ->links(
                '/posts',
                $post->url(1),
                $post->nextPageUrl(),
                $post->previousPageUrl(),
                $post->url($post->lastPage())
            )
            ->success('true')
            ->collection($post)
            ->pagination(
                $post->total(),
                $post->lastPage(),
                $post->currentPage(),
                $post->perPage()
            );

        $this->assertJson(json_encode($response));
        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('_embedded', $response);
        $this->assertArrayHasKey('name', $response);
        $this->assertArrayHasKey('page', $response);
        $this->assertArrayHasKey('pageCount', $response);
        $this->assertArrayHasKey('pageSize', $response);
        $this->assertArrayHasKey('total', $response);
        $this->assertSame(50, $response['total']);
        $this->assertSame('posts', $response['name']);
        $this->assertSame(1, $response['page']);
        $this->assertSame($pageCount, $response['pageCount']);
        $this->assertSame($perPage, $response['pageSize']);
        $this->assertTrue(is_array($response['_embedded']));
        $this->assertTrue(is_array($response['_embedded']['posts']));
    }

    public function testItWillHandelOneRecordWithRelation()
    {
        factory(Posts::class, 3)->create();
        $post = Posts::with('user')->find(1);

        $response = Hal::serviceName('post')
            ->links('/posts/1')
            ->collection($post->user, 'user')
            ->success('true')
            ->record($post);


        $this->assertJson(json_encode($response));
        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('_links', $response);
        $this->assertArrayHasKey('_embedded', $response);
        $this->assertTrue(is_array($response['_embedded']));
        $this->assertTrue(is_array($response['_embedded']['users']));
    }

    public function testMessage()
    {
        $response = Hal::serviceName('testing')
            ->message('This is a message');

        $this->assertArrayHasKey('message', $response);
        $this->assertEquals('This is a message', $response['message']);
    }

    public function testValidation()
    {
        $response = Hal::serviceName('testing')
            ->validation([
                'name' => ['This is a required field'],
                'email' => ['This should be an email']
            ]);

        $this->assertArrayHasKey('messages', $response);
        $this->assertArrayHasKey('name', $response['messages']['body']);
        $this->assertArrayHasKey('email', $response['messages']['body']);

        $this->assertEquals('This should be an email',$response['messages']['body']['email'][0]);
        $this->assertEquals('This is a required field',$response['messages']['body']['name'][0]);
    }
}
