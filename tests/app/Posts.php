<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Essence\Hal\Traits\HalModel;

class Posts extends Model
{
    use HalModel;

    protected $path = 'master-posts';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
